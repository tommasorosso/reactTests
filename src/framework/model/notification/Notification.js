class WebNotification {
    constructor(title, body, iconUrl) {
        this.tilte = title;
        this.body = body;
        this.iconUrl = iconUrl;
        Notification.requestPermission();
    }
    notify() {
        if (Notification.permission !== 'granted') {
            Notification.requestPermission();
        } else {
// eslint-disable-next-line no-new
            new Notification(this.tilte, { icon: this.iconUrl, body: this.body });
        }
    }

}
module.exports = WebNotification;
