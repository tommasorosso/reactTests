import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';
import {
    BrowserRouter as Router,
    Route,
} from 'react-router-dom';

import StarterPage from './components/StarterPage';
import counter from './reducers/reducer';
import GenericBubble from './framework/controller/GenericBubble';
import cookBubble from './components/cookBubble/cookBubble';
import clientBubble from './components/clientBubble/clientBubble';
import adminBubble from './components/adminBubble/adminBubble';
import TodoBubble from './components/todoBubble/todoBubble';
import WebNotification from './framework/model/notification/Notification';

const store = createStore(counter);
const rootEl = document.getElementById('root');

function HomeRoute() {
    return <StarterPage />;
}

function notify() {
    return (
        <div>
            <button
              onClick={() => {
                  setTimeout(() => {
                      const notifica = new WebNotification('Notification title', 'notification body', 'http://pngimg.com/uploads/frog/frog_PNG3848.png');
                      notifica.notify();
                  }, 3000);
              }}
            >notify after 3 sec</button>
        </div>
    );
}

const render = () => ReactDOM.render(
    <Router>
        <div>
            <Route exact path="/" component={HomeRoute} />
            <Route path="/bubble/" component={GenericBubble} />
            <Route path="/cook" component={cookBubble} />
            <Route path="/client" component={clientBubble} />
            <Route path="/admin" component={adminBubble} />
            <Route path="/todo/" component={TodoBubble} />
            <Route path="/notify" component={notify} />
        </div>
    </Router>,
  rootEl,
);

render();
store.subscribe(render);
